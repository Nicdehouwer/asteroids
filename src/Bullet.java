/**
 * @author Nic De Houwer
 * @version 1.0 08/07/2023 23:32
 */
public class Bullet {
    private double hSpeed;
    private double vSpeed;
    private double shotSpeed;
    private double posX;
    private double posY;
    private int age;
    private boolean alive;
    private int gameWidth;
    private int gameHeight;

    public Bullet(double posX, double posY, double shotSpeed, double horSpeed, double verSpeed, int gameWidth, int gameHeight){
        age = 15;
        alive = true;
        this.posX = posX;
        this.posY = posY;
        this.shotSpeed = shotSpeed;
        this.hSpeed = horSpeed;
        this.vSpeed = verSpeed;
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
    }
    public void move(){
        age --;
        if (age < 0){
            alive = false;
        }
        this.posX += hSpeed*shotSpeed;
        if (posX > gameWidth){
            posX -= gameWidth;
        }
        if (posX < 0){
            posX += gameWidth;
        }
        this.posY += vSpeed*shotSpeed;
        if (posY > gameHeight){
            posY -= gameHeight;
        }
        if (posY < 0){
            posY += gameHeight;
        }
    }

    public double gethSpeed() {
        return hSpeed;
    }

    public double getVerSpeed() {
        return vSpeed;
    }
    public boolean isDead(){
        return !alive;
    }

    public double getX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setX(double x) {
        this.posX = x;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public void sethSpeed(double hSpeed) {
        this.hSpeed = hSpeed;
    }

    public void setVerSpeed(double verSpeed) {
        this.vSpeed = verSpeed;
    }
}
