/**
 * @author Nic De Houwer
 * @version 1.0 09/07/2023 21:04
 */
public class Asteroid {
    private double posX;
    private double posY;
    private double hSpeed;
    private double vSpeed;
    private double rotationSpeed;
    private double size;
    private boolean hit;

    public Asteroid(double posX,double posY, double hSpeed, double vSpeed, double rotationSpeed, double size) {
        this.posX = posX;
        this.posY = posY;
        this.hSpeed = hSpeed;
        this.vSpeed = vSpeed;
        this.rotationSpeed = rotationSpeed;
        this.size = size;
        this.hit = false;
    }
    public boolean isHit(){
        return hit;
    }
    public void move(){
        this.posX += hSpeed;
        this.posY += vSpeed;
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public double gethSpeed() {
        return hSpeed;
    }

    public double getvSpeed() {
        return vSpeed;
    }

    public double getRotationSpeed() {
        return rotationSpeed;
    }

    public double getSize() {
        return size;
    }
}
