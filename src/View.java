import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * @author Nic De Houwer
 * @version 1.0 21/06/2023 11:32
 */
public class View extends BorderPane {

    Label label;
    private Canvas bulletCanvas;
    private GraphicsContext bulletGc;
    private Model model;
    private Rectangle blokje;
    private Canvas asteroidCanvas;
    private GraphicsContext asteroidGc;

    public View(Model model){
        this.model = model;
        initNodes();
        layoutNodes();
    }

    private void initNodes(){
        label = new Label("0");
        bulletCanvas = new Canvas(model.getGameWidth(),model.getGameHeight());
        asteroidCanvas = new Canvas(model.getGameWidth(),model.getGameHeight());
        bulletGc = bulletCanvas.getGraphicsContext2D();
        asteroidGc = asteroidCanvas.getGraphicsContext2D();
        blokje = new Rectangle();
    }
    private void layoutNodes(){
        blokje.setWidth(20);
        blokje.setHeight(10);
        this.getChildren().addAll(bulletCanvas, asteroidCanvas, blokje);
    }
    public void clear(){
        bulletGc.clearRect(0,0,model.getGameWidth(),model.getGameHeight());
        asteroidGc.clearRect(0,0,model.getGameWidth(),model.getGameHeight());
    }
    public void drawBlokje(){
        if (!model.isDead()) {
            blokje.setX(model.getPosX());
            blokje.setY(model.getPosY());
            blokje.rotateProperty().set(model.getAngle());
        }
    }
    public void drawBullets(){
        bulletGc.setFill(Color.RED);
        for (Bullet bullet : model.getBullets()){
            if (bullet != null && !bullet.isDead()){
                bulletGc.fillOval(bullet.getX(),bullet.getPosY(),5,5);
            }
        }
    }
    public void drawAsteroids(){
        for (Asteroid a : this.model.getAsteroids()){
            if (a != null){
                asteroidGc.setFill(Color.BROWN);
                asteroidGc.fillOval(a.getPosX(),a.getPosY(),a.getSize(),a.getSize());
            }
        }
    }

    public GraphicsContext getBulletGc(){
        return bulletGc;
    }

    public Canvas getBulletCanvas() {
        return bulletCanvas;
    }

    public Canvas getAsteroidCanvas() {
        return asteroidCanvas;
    }

    public GraphicsContext getAsteroidGc() {
        return asteroidGc;
    }
    public void explode(){
        for (int i = 50; i> 0; i--){
            bulletGc.clearRect(model.getPosX()-(i/2),model.getPosY()-(i/2),model.getPosX()+(i/2), model.getPosY()+(i/2));
            bulletGc.setFill(Color.ORANGE);
            Glow glow = new Glow(15);
            bulletGc.applyEffect(glow);
            bulletGc.fillOval(model.getPosX(), model.getPosY(), i, i);
        }
    }

    public Rectangle getBlokje(){
        return blokje;
    }

}
