import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Nic De Houwer
 * @version 1.0 09/07/2023 21:06
 */
public class AsteroidsRepository {
    private List<Asteroid> asteroids;
    private int asteroidIndex;
    static private Random random;

    public AsteroidsRepository() {
        asteroids = new ArrayList<>();
        asteroidIndex = 0;
        random = new Random();
    }
    public void startWave(int waveSize){
        System.out.println("Wave Started");
        for (int i = 0; i<waveSize; i++){
            asteroids.add(new Asteroid(random.nextDouble(400), random.nextDouble(400),
                    random.nextDouble(1), random.nextDouble(1), 3,30));
        }
    }
    public void moveAsteroids(){
        for (Asteroid a : asteroids){
            a.move();
            if (a.getPosX() < -10 || a.getPosX() > 410
            || a.getPosY() < -10 || a.getPosY() > 410){
                a = null;
            }

        }
    }

    public List<Asteroid> getAsteroids() {
        return asteroids;
    }
}
