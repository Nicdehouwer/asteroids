import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.awt.event.KeyEvent;

/**
 * @author Nic De Houwer
 * @version 1.0 21/06/2023 11:36
 */
public class Presenter {
    Model model;
    View view;
    private Timeline timeLine;

    public Presenter(Model model, View view){
        this.model = model;
        this.view = view;
        addEventListeners();
        timeLine = new Timeline(new KeyFrame(Duration.millis(this.model.getFrameRate()), actionEvent -> updateView()));
        timeLine.setCycleCount(Animation.INDEFINITE);
        timeLine.play();
    }
    public void addEventListeners() {
        addKeyevents();
        addCollisionEvents();
    }
    private void updateView(){
        model.increaseWaveStarter();
        if (model.getWaveStarter() > model.getWaveThreshold()){
            model.deployWave();
        }
        model.move();
        for (Bullet bullet : model.getBullets()){
            if (bullet != null){
                bullet.move();
            }
        }
        model.getAsteroidRepo().moveAsteroids();
        view.clear();
        view.drawBlokje();
        view.drawBullets();
        view.drawAsteroids();
        addCollisionEvents();
    }

    private void addKeyevents(){
        this.view.getBlokje().setFocusTraversable(true);
        this.view.getBlokje().setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.RIGHT){
                model.rotate(20);
            }
            if (keyEvent.getCode() == KeyCode.LEFT){
                model.rotate(-20);
            }
            if (keyEvent.getCode() == KeyCode.UP){
                model.sethSpeed(Math.cos(Math.toRadians(model.getAngle())));
                model.setvSpeed(Math.sin(Math.toRadians(model.getAngle())));
            }
            if (keyEvent.getCode() == KeyCode.SPACE){
                model.shoot();
            }
        });
    }

    private void addCollisionEvents(){
        for (Asteroid a : this.model.getAsteroids()){
            if (Math.abs(this.model.getPosX() - a.getPosX()) < (a.getSize()-5) &&
                    Math.abs(this.model.getPosY() - a.getPosY()) < (a.getSize())-5 ){
                this.model.explode();
                this.view.explode();
                timeLine.stop();
            }
        }
    }

}
