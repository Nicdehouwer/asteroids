import javafx.geometry.Bounds;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Nic De Houwer
 * @version 1.0 21/06/2023 11:40
 */
public class Model {
    private int gameWidth;
    private int gameHeight;
    private double posX;
    private double posY;
    private double angle;
    private double vSpeed;
    private double hSpeed;
    private double frameRate;
    private double maxSpeed;
    private double bulletSpeed;
    public boolean bulletExists;
    private Bullet[] bullets;
    private int maxBullets;
    private int bulletIndex;
    private AsteroidsRepository asteroidRepo;
    private List<Asteroid> asteroids;
    private int waveStarter;
    private int waveThreshold;
    private int waveSize;
    private boolean dead;

    public Model(){
        gameWidth = 400;
        gameHeight = 400;
        bulletIndex = 0;
        maxBullets = 10;
        posX = gameWidth/2;
        posY = gameHeight/2;
        angle = 270;
        frameRate = 30;
        vSpeed = 0;
        hSpeed = 0;
        maxSpeed = 10;
        bulletSpeed = 20;
        bulletExists = true;
        bullets = new Bullet[maxBullets];
        asteroidRepo = new AsteroidsRepository();
        asteroids = asteroidRepo.getAsteroids();
        waveStarter = 0;
        waveThreshold = 200;
        waveSize = 2;
        dead = false;
    }
    public void rotate(int i){
        angle = angle +i;
        if (angle > 360){
            angle = angle - 360;
        }
        if (angle < 0){
            angle = angle + 360;
        }
    }
    public void shoot(){
        if (!dead) {
            bulletIndex++;
            if (bulletIndex >= maxBullets) {
                bulletIndex = 0;
            }
            bullets[bulletIndex] = new Bullet(posX, posY, bulletSpeed, Math.cos(Math.toRadians(angle)),
                    Math.sin(Math.toRadians(angle)), gameWidth, gameHeight);
        }
    }
    public void move(){
        if (!dead) {
            this.posX += hSpeed;
            if (posX > gameWidth) {
                posX -= gameWidth;
            }
            if (posX < 0) {
                posX += gameWidth;
            }
            this.posY += vSpeed;
            if (posY > gameHeight) {
                posY -= gameHeight;
            }
            if (posY < 0) {
                posY += gameHeight;
            }
        }
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public double getFrameRate() {
        return frameRate;
    }

    public double getAngle() {
        return angle;
    }
    public void setvSpeed (double x){
        if (vSpeed < maxSpeed){
            vSpeed += x;
        }
    }
    public void sethSpeed (double x){
        if (hSpeed < maxSpeed){
            hSpeed += x;
        }
    }
    public Bullet[] getBullets() {
        return bullets;
    }
    public int getGameWidth() {
        return gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public int getWaveStarter() {
        return waveStarter;
    }
    public void increaseWaveStarter(){
        waveStarter++;
    }
    public int getWaveThreshold(){
        return waveThreshold;
    }
    public void deployWave(){
        waveStarter = 0;
//        waveThreshold += 200;
        asteroidRepo.startWave(waveSize);
        waveSize += 1;
    }
    public void explode(){
        dead = true;
        System.out.println("BOOM");
    }
    public List<Asteroid> getAsteroids(){
        return asteroids;
    }

    public AsteroidsRepository getAsteroidRepo() {
        return asteroidRepo;
    }

    public boolean isDead() {
        return dead;
    }
}
