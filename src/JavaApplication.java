import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Nic De Houwer
 * @version 1.0 21/06/2023 11:29
 */
public class JavaApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) {
        Model model = new Model();
        View view = new View (model);
        new Presenter (model, view);
        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.setHeight(model.getGameHeight());
        stage.setWidth(model.getGameWidth());
        stage.show();
    }
}
